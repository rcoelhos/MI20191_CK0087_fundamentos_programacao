#include <stdio.h>
#include <stdlib.h>

float AcessarElementoMatriz(int dim, float *matriz, int posLinha, int posColuna);

int main()
{
    int dimVetor, dimMatSim, posLinha, posColuna;
    float resultado;
    float *matriz;

    printf("\nDigite a dimensao da matriz simetrica: ");
    scanf("%d", &dimMatSim);

    dimVetor = (dimMatSim * dimMatSim + dimMatSim) / 2;

    matriz = (float*)malloc(dimVetor * sizeof( float ));

    for(int ii = 0; ii < dimVetor; ii++)
    {
        matriz[ ii ] = (float) ii / 2;
    }

    for(int ii = 0; ii < dimVetor; ii++)
    {
        printf("%f\t", matriz[ ii ]);
    }

    printf("\nDigite a posicao da linha a ser acessada: ");
    scanf("%d", &posLinha);
    printf("\nDigite a posicao da coluna a ser acessada: ");
    scanf("%d", &posColuna);
    resultado = AcessarElementoMatriz(dimMatSim, matriz, posLinha, posColuna);

    printf("A posicao (%d,%d) da matriz simetrica e: %f", posLinha, posColuna, resultado);

    return 0;
}

float AcessarElementoMatriz(int dim, float *matriz, int posLinha, int posColuna)
{
    int k;

    if(posLinha >= posColuna)
        k = (posLinha - 1) * posLinha / 2 + (posColuna - 1);
    else
        k = (posColuna - 1) * posColuna / 2 + (posLinha - 1);

    return matriz[ k ];
}