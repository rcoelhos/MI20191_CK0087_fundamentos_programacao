/* 
 * Sexto codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

int main () {
    // Declaracao de variaveis
    int numero1, numero2;

    // Solicitando informacao ao usuario referente ao primeiro numero
    printf ("\n\nDigite o primeiro numero inteiro :");
    scanf ("%d" ,&numero1);
    // Solicitando informacao ao usuario referente ao segundo numero
    printf ("\n\nDigite o segundo numero inteiro :");
    scanf ("%d" ,&numero2);
    
    // Verificar se os numeros sao pares
    if (numero1%2 == 0 && numero2%2 == 0) {
        printf("\n\nOs numeros fornecidos sao pares !\n");
    } else if (numero1%2 == 0 || numero2%2 == 0) {
        printf("\n\nPelo menos um dos numeros fornecidos eh par !\n");
    } else {
        printf("\n\nOs numeros fornecidos sao impares !\n");
    }
    
    return 0;
}