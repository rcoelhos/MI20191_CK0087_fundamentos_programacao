/* 
 * Decimo oitavo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Subrotina destinada a trocar o conteudo de dois ponteiros
void Troca(int *a, int *b)
{
    int aux;

    aux = *b;
    *b = *a;
    *a = aux;
}

int main()
{
    int a = 7, b = 5;

    Troca( &a, &b );

    printf("\nO valor de a eh: %d", a);
    printf("\nO valor de b eh: %d", b);

    return 0;
}