#include <stdio.h>  // biblioteca para operacoes de entrada e saida
#include <stdlib.h> // biblioteca para gerenciamento de memoria dinamica ...
#include <string.h> // biblioteca para manipulacao de 'strings'

int main()
{
    // Criando a viariaveis cidades, que sao duas cadeias de caracteres (vetor de char)
    char cidade1[30] = "Rio de Janeiro";        // comprimento maximo da cadeia esta definido
    char cidade2[ ] = "Rio Grande do Norte";    // comprimento maximo da cadeia nao esta definido
    char* cidade3 = "Rio Grande do Sul";        // uso de ponteiro para enderecar essa cadeia
    char* cidadeAux;                            // alocacao de memoria para um ponteiro de char
    // Alocando memoria para uma variavel com valor inteiro
    int resposta;

    // imprimir na tela o valor armazenado na variavel 'cidade1'
    printf("%s\n", cidade1);
    // imprimir na tela o comprimento da cadeia de caracteres da variavel 'cidade1'
    printf("O tamanho dessa string eh: %d\n", strlen( cidade1 ));
    
    // alocacao dinamica para o ponteiro 'cidadeAux' com o comprimento de 'cidade1'
    cidadeAux = (char*)malloc((strlen( cidade1 ) + 1) * sizeof( char ));
    // copiar o conteudo da viariavel 'cidade1' no espaco de memoria enderecado por 'cidadeAux'
    strcpy(cidadeAux, cidade1);
    
    // imprimir na tela o valor armazenado na variavel 'cidadeAux'
    printf("%s\n", cidadeAux);
    // imprimir na tela o comprimento da cadeia de caracteres da variavel 'cidadeAux'
    printf("O tamanho da string auxiliar eh: %d\n", strlen( cidadeAux ));
    
    // imprimir na tela o valor armazenado na variavel 'cidade2'
    printf("%s\n", cidade2);
    // imprimir na tela o comprimento da cadeia de caracteres da variavel 'cidade2'
    printf("O tamanho dessa string eh: %d\n", strlen( cidade2 ));

    // imprimir na tela o valor da memoria enderecada pelo ponteiro 'cidade3'
    printf("%s\n", cidade3);
    // imprimir na tela o comprimento da cadeia de caracteres do ponteiro 'cidade3'
    printf("O tamanho dessa string eh: %d\n", strlen( cidade3 ));

    // imprimir na tela o tamanho de memoria alocada para a variavel 'cidade1'
    printf("O tamanho da variavel cidade1 eh: %d\n", sizeof( cidade1 ));
    // imprimir na tela o tamanho de memoria alocada para a variavel 'cidade2'
    printf("O tamanho da variavel cidade2 eh: %d\n", sizeof( cidade2 ));
    // imprimir na tela o tamanho de memoria alocada para o ponteiro 'cidade3'
    printf("O tamanho da variavel cidade3 eh: %d\n", sizeof( cidade3 ));
    // imprimir na tela o tamanho de memoria alocada para o ponteiro 'cidade3'
    printf("O tamanho da variavel cidadeAux eh: %d\n", sizeof( cidadeAux ));

    // alterando o caracter na primeira posicao da cadeia
    cidade1[ 0 ] = 'r';
    // imprimir na tela o conteudo atualizado da cadeia de caracteres
    printf("%s\n", cidade1);

    // alterando o caracter na primeira posicao da cadeia
    cidade2[ 0 ] = 'r';
    // imprimir na tela o conteudo atualizado da cadeia de caracteres
    printf("%s\n", cidade2);

    // imprimir na tela o conteudo atualizado da cadeia de caracteres
    printf("Antes da manipulacao: %s\n", cidade3);
    // ERRO: tentando alterar o conteudo na primeira posicao do ponteiro
    strncpy(cidade3, "r", 1);
    // imprimir na tela o conteudo atualizado da cadeia de caracteres
    printf("Depois da primeira manipulacao: %s\n", cidade3);
    // ERRO: tentando alterar o conteudo na primeira posicao do ponteiro
    cidade3[ 0 ] = 'r';
    // imprimir na tela o conteudo atualizado da cadeia de caracteres
    printf("Depois da segunda manipulacao: %s\n", cidade3);

    return 0;
}