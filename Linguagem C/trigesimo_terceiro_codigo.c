#include <stdio.h>

// Descricao do prototipo
int comprimento ( char * );
void copia ( char *, char * );

int main()
{
    // Criando a viariavel cidades, que eh um vetor de char
    /*char cidades[10];

    cidades[0] = 'F';
    cidades[1] = 'o';
    cidades[2] = 'r';
    cidades[3] = 't';
    cidades[4] = 'a';
    cidades[5] = 'l';
    cidades[6] = 'e';
    cidades[7] = 'z';
    cidades[8] = 'a';
    cidades[9] = '\0';
    */
    // Criando a variavel cidade, como um vetor de caracteres
    //char cidades[10] = {'F','o','r','t','a','l','e','z','a','\0'};
    char cidades[30] = "Fortaleza eh 1 cidade do Ceara";

    printf("Codigo da letra F maiuscula %d", (int)cidades[0]);
    printf("\nO caracter armaazenado eh: %c", cidades[0]);

    printf("\n%s", cidades);

    printf("\n");
    for ( int ii = 0; cidades[ii] != '\0'; ii++) 
    {
        printf ("%c", cidades[ii]);
    }
    printf ("\n");

    printf("O algarismo %c armazenado na variavel eh: %d", cidades[13], (int)cidades[13]);

    printf("\nO tamanho da frase eh: %d", comprimento( cidades ));

    return 0;
}

int comprimento ( char * s ) {
    int i;
    int n = 0;
    for (i = 0; s[i] != '\0'; i++) 
    {
        n++;
    }
    return n;
}

void copia ( char * dest , char * orig )
{
    int i;

    for (i = 0; orig [i] != '\0'; i++) {
        dest [i] = orig [i];
    }
    dest [i] = '\0';
}