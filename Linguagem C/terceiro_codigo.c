/* 
 * Terceiro codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */ 

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Programa que calcula a area e perimetro a partir da altura e largura de um retangulo,
// sendo a altura e largura ja definidas a priori
int main(void) {
    // Alocando memoria para qutro variaveis inteiras
    int altura_int = 13;            // variavel que armazena na variaavel "altura" do retangulo
    int largura_int = 25;           // variavel que armazena na variavel "largura" do retangulo
    int area_int, perimetro_int;    // variaveis que armazenarao os calculos de area e perimetro

    // Calculo matematico para obter o valor da variavel "area" desse retangulo
    area_int = altura_int * largura_int;
    // Calculo matematico para obter o valor da variavel "perimetro" desse retangulo
    perimetro_int = 2 * ( altura_int + largura_int );

    // Funcao da biblioteca <stdio.h> para imprimir informacao na tela
    printf("area do retangulo = %d \n", area_int);              // impressao do valor da variavel "area"
    printf("perimetro do retangulo = %d \n", perimetro_int);    // impressao do valor do variavel "perimetro"

    return 0;
}