/* 
 * Quinto codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Programa que converte as medidas em metros nas medidas de pes e polegadas
int main(void) {
    int pes;
    float altura, polegadas, resto;
    float converson_pes = 30.48, conversor_pol = 2.54;

    // Funcoes da biblioteca <stdio.h> para entrada e saida de informacao
    printf("\n\nDigite a altura: ");    // impressao de um texto na tela
    scanf("%f", &altura);               // armazenar valor digitado pelo usuario na variavel "altura"

    // Converte a medida em metros da variavel "altura" para centimetros
    altura = 100 * altura;

    // Calcular a parte inteira da quantidade de pes referente a altura em centimentros
    pes = (int)(altura / converson_pes);
    // Armazenar somente a parte decimal da conversao de centimentos em pes
    resto = (altura / converson_pes) - pes;
    // Calcular a quantidade de polegadas equivalente a parte decimal da conversao acima
    polegadas = (resto * converson_pes) / conversor_pol;

    // impressao na tela o resultado obtido 
    printf("\n\nAltura e: %d ft %.1f pol", pes, polegadas);

    return 0;
}