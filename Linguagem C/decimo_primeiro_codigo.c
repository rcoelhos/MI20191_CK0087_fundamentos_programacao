/* 
 * Decimo primeiro codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// prototipo da funcao que calcula o fatorial de um numero inteiro
int fatorial( int ); //essa linha chama-se prototipo

int main(void) {
    int numero; //declaracao das variaveis

    // Solicitar ao usuario um numero inteiro nao negativo
    printf("Digite um numero nao negativo: ");
    scanf("%d", &numero);
    
    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo
    printf ("Fatorial de %d = %d \n", numero, fatorial( numero ));

    return 0;
} // Fim da funcao MAIN

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "for" e iterador crescente
int fatorial( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;
} // Fim da funcao FATORIAL