/*  
 *  Primeiro codigo em Linguagem C
 *  Curso: Matematica Industrial
 *  Disciplina: Fundamento de Programacao
 *  Prof.: Ricardo Coelho
 *  Versao 0.2
*/

#include <stdio.h> // Biblioteca padrao da Linguagem C para funcoes de entrada e saida

int main() {
    int a;              // alocar memoria para a variavel 'a' inteira
    int b;              // alocar memoria para a variavel 'b' inteira
    float c;            // alocar memoria para a variavel 'c' float

    a = 5;              // atribuir valor a variavel 'a' alocada
    b = 10;             // atribuir valor a variavel 'b' alocada
    c = 5.3;            // atribuir valor a varaivel 'c' alocada

    printf("%d\n", a);  // imprimir na tela o valor da variavel 'a'
    printf("%d\n", b);  // imprimir na tela o valor da variável 'b'
    printf("%.3f", c);  // imprimir na tela o valor da variavel 'c'

    return 0;
}
