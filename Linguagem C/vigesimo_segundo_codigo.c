/* 
 * Vigesimo segundo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida
#include <math.h>  // biblioteca para operacoes elementares de matematica

// Declaracao de macro para definir um valor aproximado de PI
#define PI 3.14159

// Subrotina para calcular a area de uma circuferencia
float area_circ(float raioCirc)
{
    // Expressao matematica para nao usar a biblioteca <math.h> para
    // calcular a potencia da medida do raio na equacao da area da circunferencia
    //return PI * raioCirc * raioCirc;

    // Uso da subrotina de potenciacao que esta implementada dentro da
    // biblioteca <math.h>, que e padrao da linguagem C
    return PI * pow(raioCirc,2);
}

// Subrotina para calcular o comprimento da circuferencia
float comprimento_circ(float raioCirc)
{
    return 2 * PI * raioCirc;
}

// Funcao principal
int main()
{
    float raio;

    printf("\nDigite o valor do raio da circunferencia: ");
    scanf("%f", &raio);

    printf("\nO comprimento da circuferencia de raio %f eh: %.2f", raio, comprimento_circ(raio));

    printf("\nO area da circuferencia de raio %f eh: %.2f", raio, area_circ(raio));

    return 0;
}