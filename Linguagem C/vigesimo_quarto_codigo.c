#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float Media ( int, float* );
float Variancia ( int, float*, float );

int main()
{
    int i;
    float notas[5];
    float media, variancia;

    notas[0] = 7.5;
    notas[1] = 8.0;
    notas[2] = 4.3;
    notas[3] = 2.5;
    notas[4] = 10.0;
/*
    for( i = 0; i < 5; i++)
    {
        printf("\nDigite o valor da nota a ser calculada: ");
        scanf("%f", &notas[ i ]);
    }
*/
    media = Media( 5, notas );
    variancia = Variancia( 5, notas, media );

    printf("\nA media das notas e: %.2f", media);
    printf("\nA variancia das notas e: %.2f", variancia);
    printf("\nO desvio padrao das notas e: %.2f", sqrt(variancia));

    return 0;
}

/* calculo da media */
float Media ( int n, float * v) {
int i;
float s = 0.0;
for (i = 0; i< n; i++)
s += v[i];
return s/n;
}

/* calculo da variancia */
float Variancia (int n, float * v, float m) {
int i;
float s = 0.0;
for (i = 0; i< n; i++)
s += (v[i] - m)*(v[i] - m);
return s/n;
}