#include <stdio.h>

// Descricao do prototipo
int comprimento ( char * );
void copia ( char *, char * );
void concatena ( char *, char * );
int compara ( char *, char * );

int main()
{
    // Criando a viariaveis cidades, que sao dois vetores de char
    char cidadeOrigem[30] = "Fortaleza ";
    char cidadeDestino[30] = "Florianopolis";
    char cidadeAuxiliar[30] = "Fortaleza";
    int resposta;

    printf("%s\n", cidadeOrigem);
    printf("%s\n", cidadeDestino);

    //resposta = compara( cidadeOrigem, cidadeDestino );
    resposta = compara( cidadeAuxiliar, cidadeOrigem );
    printf("O resultado da comparacao entre as strings eh: %d\n", resposta);

    concatena( cidadeOrigem, cidadeDestino );
    resposta = compara( cidadeAuxiliar, cidadeOrigem );
    printf("O resultado da comparacao entre as strings eh: %d\n", resposta);

    printf("%s\n", cidadeOrigem);
    printf("%s\n", cidadeDestino);

    return 0;
}

int comprimento ( char * s ) {
    int i;
    int n = 0;
    for (i = 0; s[i] != '\0'; i++) 
    {
        n++;
    }
    return n;
}

void copia ( char * dest , char * orig )
{
    int i;

    for (i = 0; orig [i] != '\0'; i++) {
        dest [i] = orig [i];
    }
    dest [i] = '\0';
}

void concatena ( char * dest, char * orig )
{
    int ii = 0, jj;

    while (dest[ ii ] != '\0') ii++;

    for (jj = 0; orig[ jj ] != '\0'; jj++)
    {
        dest [ ii ] = orig[ jj ];
        ii++;
    }

    dest[ ii ] = '\0';
}

int compara ( char * s1 , char * s2)
{
    int i;

    for (i = 0; s1[ i ] != '\0' && s2[ i ] != '\0'; i++) 
    {
        if (s1[i]<s2[i])
            return -1;
        else if (s1[i]>s2[i])
            return 1;
    }

    if (s1[i] == s2[i])
        return 0;
    else if (s2[i]!= '\0')
        return -2;
    else
        return 2;
}