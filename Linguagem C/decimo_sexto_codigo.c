/* 
 * Decimo sexto codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

int main () 
{
    int qualquer, nenhuma; /*variaveis inteiras*/
    int *ponteiro; /*variavel ponteiro para inteiro*/
    
    //qualquer = 5; /* a recebe o valor 5 */
    printf("\nValor da viariavel qualquer e: %d\n: ", qualquer);

    ponteiro = &qualquer; /* p recebe o endereço de a */
    printf("\nValor do ponteiro ponteiro e: %d\n", *ponteiro);

    *ponteiro = 6; /*conteudo de p recebe o valor 6 */
    printf("\nValor do ponteiro ponteiro: %d\n", *ponteiro);
    printf("\nValor da variavel qualquer: %d\n", qualquer);

    qualquer = 9; /* a recebe novo valor 9 */
    printf("\nValor da variavel qualquer: %d\n", qualquer);
    printf("\nValor do ponteiro ponteiro: %d\n", *ponteiro);

    nenhuma = 7; /* recebe valor 7 */
    ponteiro = &nenhuma; /* ponteiro recebe endereco de memoria da variavel nenhuma */
    printf("\nValor da variavel qualquer: %d\n", qualquer);
    printf("\nValor do ponteiro ponteiro: %d\n", *ponteiro);
    printf("\nValor da variavel nenhuma: %d\n", nenhuma);

    *ponteiro = 3; /* recebe novo valor 3 */
    printf("\nValor da variavel qualquer: %d\n", qualquer);
    printf("\nValor do ponteiro ponteiro: %d\n", *ponteiro);
    printf("\nValor da variavel nenhuma: %d\n", nenhuma);

    return 0;
}