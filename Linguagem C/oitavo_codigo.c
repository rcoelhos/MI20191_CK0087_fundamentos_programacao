/* 
 * Oitavo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Programa que calcula o fatorial de um numero inteiro
// fornecido pelo usuario, usando o comando "for"
int main (void) {
  int iterador, numero, fatorial;   // alocando memoria para variaveis inteiras
  fatorial = 1;                     // designando valor para uma variavel inteira

  // Solicitando informacao ao usuario
  printf("Digite um inteiro nao negativo: ");
  scanf("%d", &numero);  

  // Estrutura de repeticao usando o comando "for"
  for(size_t iterador = 1; iterador <= numero; iterador++)
  {
    fatorial *= iterador;
  }

  /*  Alternativa para calculo do fatorial de um valor inteiro
      nao negativo usando o comando "for" com iterador regressivo

    for(iterador = numero; iterador > 0; --iterador) {
        //fatorial = fatorial * iterador;
        fatorial *= iterador;
        printf("%d\t", fatorial);
    }
*/
  
  printf("Fatorial de n = %d \n", fatorial);
  
  return 0;
} 