#include <stdio.h>

#define M 3
#define N 5

void AtribuirValorMatriz(int dimLinhas, int dimColunas, int (*matriz)[dimColunas] );
void ImprimirValorMatriz(int dimLinhas, int dimColunas, int matriz[dimLinhas][dimColunas] );
int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"

int main()
{
    int matriz[M][N];

    AtribuirValorMatriz( M, N, matriz);
    ImprimirValorMatriz( M, N, matriz);

    return 0;
}

void AtribuirValorMatriz(int dimLinhas, int dimColunas, int (*matriz)[dimColunas] )
{
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        for(int jj = 0; jj < dimColunas; jj++)
        {
            //matriz[ii][jj] = (ii + jj) % 4;
            matriz[ii][jj] = fatorial_for( ii + jj );
        }
    }
}

void ImprimirValorMatriz(int dimLinhas, int dimColunas, int matriz[dimLinhas][dimColunas] )
{
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        printf("Elementos da linha %d\n", ii);
        for(int jj = 0; jj < dimColunas; jj++)
        {
            printf("%d\t",matriz[ii][jj]);
        }
        printf("\n");
    }
}

int fatorial_for( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;

    //printf ("Fatorial de n = %d \n", f);
}