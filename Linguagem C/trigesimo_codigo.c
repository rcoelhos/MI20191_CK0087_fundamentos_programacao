#include <stdio.h>
#include <stdlib.h>

#define M 3
#define N 5

void AtribuirValorMatriz(int dimLinhas, int dimColunas, int (*matriz)[dimColunas] );

void AtribuirValorMatrizVetor(int dimLinhas, int dimColunas, int (*matriz)[dimColunas], int *vetor );

//void ImprimirValorMatriz(int dimLinhas, int dimColunas, int matriz[dimLinhas][dimColunas] );
void ImprimirValorMatriz(int dimLinhas, int dimColunas, int** matriz );

void ImprimirValorVetor(int dimLinhas, int dimColunas, int matriz[dimLinhas*dimColunas] );

int* TranspostaVetor(int dimLinhas, int dimColunas, int* matriz);

int** TranspostaMatriz(int dimLinhas, int dimColunas, int** matriz);

int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"

int main()
{
    //int matriz[M][N] = {{1,2,3,4,5},{3,5,7,9,11},{2,4,6,8,10}};
    //int *vetor, *vetorAuxiliar;
    int **matriz, **matrizAuxiliar;

    matriz = (int**)malloc(M * sizeof( int* ));
    for(int ii = 0; ii < M; ii++)
    {
        matriz[ii] = (int*)malloc(N * sizeof( int ));
    }

    for(int ii = 0; ii <  M; ii++)
    {
        for(int jj = 0; jj < N; jj++)
        {
            matriz[ii][jj] = fatorial_for(ii + jj);
        }
    }

    //vetor = (int*)malloc(M * N * sizeof(int));

    //AtribuirValorMatriz( M, N, matriz );
    //AtribuirValorMatrizVetor( M, N, matriz, vetor);
    ImprimirValorMatriz( M, N, matriz);
    //ImprimirValorVetor(M, N, vetor);
    //vetorAuxiliar = TranspostaMatriz(M, N, vetor);
    printf("\nImprimindo a transposta\n");
    //ImprimirValorVetor(N, M, vetorAuxiliar);
    
    matrizAuxiliar = TranspostaMatriz(M, N, matriz);
    ImprimirValorMatriz(N, M, matrizAuxiliar);

    return 0;
}

void AtribuirValorMatriz(int dimLinhas, int dimColunas, int (*matriz)[dimColunas] )
{
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        for(int jj = 0; jj < dimColunas; jj++)
        {
            //matriz[ii][jj] = (ii + jj) % 4;
            matriz[ii][jj] = fatorial_for( ii + jj );
        }
    }
}

void AtribuirValorMatrizVetor(int dimLinhas, int dimColunas, int (*matriz)[dimColunas], int *vetor )
{
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        for(int jj = 0; jj < dimColunas; jj++)
        {
            //matriz[ii][jj] = (ii + jj) % 4;
            vetor[ii*dimColunas + jj] = matriz[ii][jj];
        }
    }
}

//void ImprimirValorMatriz(int dimLinhas, int dimColunas, int matriz[dimLinhas][dimColunas] )
void ImprimirValorMatriz(int dimLinhas, int dimColunas, int** matriz )
{
    printf("\nImprimindo valores da matriz\n");
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        printf("Elementos da linha %d\n", ii);
        for(int jj = 0; jj < dimColunas; jj++)
        {
            printf("%d\t",matriz[ii][jj]);
        }
        printf("\n");
    }
}

void ImprimirValorVetor(int dimLinhas, int dimColunas, int vetor[dimLinhas*dimColunas] )
{
    printf("\nImprimindo valores do vetor\n");
    for(int ii = 0; ii < dimLinhas; ii++)
    {
        printf("Elementos da linha %d\n", ii);
        for(int jj = 0; jj < dimColunas; jj++)
        {
            printf("%d\t",vetor[ii*dimColunas + jj]);
        }
        printf("\n");
    }
}

int* TranspostaVetor(int dimLinhas, int dimColunas, int* matriz)
{
    int* trp;

    trp = (int*) malloc (dimLinhas * dimColunas * sizeof ( int ));

    for (int ii = 0; ii < dimLinhas; ii++) {
        for (int jj = 0; jj < dimColunas; jj++) {
            trp [jj*dimLinhas + ii] = matriz[ii * dimColunas + jj];
        }
    }
    return trp;
}

int** TranspostaMatriz(int dimLinhas, int dimColunas, int** matriz)
{
    int** trp;

    trp = (int**) malloc (dimColunas * sizeof ( int* ));
    for (int ii = 0; ii < dimColunas; ii++)
    {
        trp[ ii ] = (int*) malloc (dimLinhas * sizeof( int ));
    }

    for (int ii = 0; ii < dimColunas; ii++) {
        for (int jj = 0; jj < dimLinhas; jj++) {
            trp[ii][jj] = matriz[jj][ii];
        }
    }
    return trp;
}

int fatorial_for( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;

    //printf ("Fatorial de n = %d \n", f);
}