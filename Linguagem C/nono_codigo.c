/* 
 * Nono codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Programa que calcula o fatorial de um numero inteiro
// fornecido pelo usuario, usando o comando "while"
int main(void) {
    int iterador, fatorial, numero; // alocar memoria para variaveis inteiras
    int contador = 0;               // alocar memoria para variavel inteira

    fatorial = 1;                   // designar valor a variavel, que equivalente a 0!
    
    // Estrutura de repeticao para obrigar ao usuario a informar um valor
    // inteiro nao negativo para calcular o fatorial desse valor informado
    do {
        printf("Digite um numero inteiro nao negativo: ");
        scanf("%d", &numero);
        contador++;
        // Estrutura de condicional para avaliar se o valor informado
        // e um valor valido ou nao
        if(contador%5 == 0) {
            printf("Esta digitando errado!\n");
        }
    }while(numero < 0 || contador <= 10);

    // Estrutura de repeticao usando o comando "while"
    iterador = 1;
    while(iterador <= numero) {
        fatorial *= iterador;
        iterador++;
    }

    printf("\nO valor do fatorial de %d e: %d", numero, fatorial);

    return 0;
}