/* 
 * Setimo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

int main(void) {
    float temperatura;      // Alocando memoria para uma variavel real

    // Solicitando informacao ao usuario referente a temperatura
    printf("Digite a temperatura atual: ");
    scanf("%f", &temperatura);

    // Analisar de forma encadeada a temperatura informada
    if(temperatura < 10) {
        printf("\n\nA temperatura esta muito fria");
    } else if(temperatura < 20) {
        printf("\n\nA temperatura esta fria");
    } else if(temperatura < 30) {
        printf("\n\nA temperatura e agradavel");
    } else {
        printf("\n\nA temperatura esta muito quente");
    }

/*  Essa alternativa nao representa a mesma solucao da anterior,
    pois nao ha um encadeamento dos condicionais

    if(temperatura < 10) {
        printf("\n\nA temperatura esta muito fria");
    }
    if(temperatura >= 10 && temperatura < 20) {
        printf("\n\nA temperatura esta fria");
    }
    if(temperatura >= 20 && temperatura < 30) {
        printf("\n\nA temperatura e agradavel");
    } else {
        printf("\n\nA temperatura esta muito quente");
    }
*/
    return 0;
}