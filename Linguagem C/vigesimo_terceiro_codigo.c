/* 
 * Vigesimo terceiro codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida
#include <math.h>  // biblioteca para operacoes elementares de matematica

// Declaracao de macro para definir operacoes de maximo e minimo
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))


// Funcao principal
int main()
{
    float primeiroNumero;
    float segundoNumero;

    printf("\nDigite o primeiro valor: ");
    scanf("%c", &primeiroNumero);

    printf("\nDigite o segundo valor: ");
    scanf("%f", &segundoNumero);

    printf("\nO maior deles eh: %c", MAX(primeiroNumero,segundoNumero));

    printf("\nO menor deles eh: %f", MIN(primeiroNumero,segundoNumero));

    return 0;
}