#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float Media ( int, float* );
float Variancia ( int, float*, float );
int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"

int main()
{
    int i, quantidadeNotas;
    float *notas;
    float media, variancia;

    //int aux[quantidadeNotas];

    printf("\nInforme o numero de notas a serem calculadas: ");
    scanf("%d", &quantidadeNotas);

    notas = malloc(quantidadeNotas * 1);
    //notas = (float *)malloc(quantidadeNotas * sizeof(float));

    notas[0] = 7.5;
    notas[1] = 8.0;
    notas[2] = 4.3;
    notas[3] = 2.5;
    notas[4] = 10.0;
/*
    for( i = 0; i < 5; i++)
    {
        printf("\nDigite o valor da nota a ser calculada: ");
        scanf("%f", &notas[ i ]);
    }
*/
    media = Media( quantidadeNotas, notas );
    variancia = Variancia( quantidadeNotas, notas, media );

    printf("\nA media das notas e: %.2f", media);
    printf("\nA variancia das notas e: %.2f", variancia);
    //printf("\nO desvio padrao das notas e: %.2f", sqrt(variancia));

    //if (notas) printf("\n%d\n%f", 0, notas[0]);
    //if (notas) 
    free (notas);

    for(int i = 0; i < quantidadeNotas; i++)
    {
    	printf("\n%f", notas[i]);
    }

    //if (notas) printf("\n%d\n%f", 0, notas[0]);

    notas = (float *)malloc(fatorial_for(18) * sizeof(float));
    if(notas == NULL)
    {
        printf("\nNao ha memoria suficiente a ser alocada para essa vetor\n");
        exit(1);
    }

	printf("\n");
    
    return 0;
}

/* calculo da media */
float Media ( int n, float * v) {
int i;
float s = 0.0;
for (i = 0; i< n; i++)
s += v[i];
return s/n;
}

/* calculo da variancia */
float Variancia (int n, float * v, float m) {
    int i;
    float s = 0.0;
    for (i = 0; i< n; i++)
        s += (v[i] - m)*(v[i] - m);
    return s/n;
}

int fatorial_for( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;

    //printf ("Fatorial de n = %d \n", f);
}
