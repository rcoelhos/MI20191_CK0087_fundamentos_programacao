/* 
 * Decimo quinto codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Declaracao dos prototipos
int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"
int fatorial_while( int ); //essa linha chama-se prototipo com o laco usando "while"
int fatorial_rec( int ); //essa linha chama-se prototipo com recursividade

int main(void) {
    int numero1, numero2, arranjo; //declaracao das variaveis

    // Solicitar ao usuario o primeiro numero inteiro nao negativo
    printf("\nDigite o primeiro numero inteiro nao negativo: ");
    scanf("%d", &numero1);

    // Solicitar ao usuario o segundo numero inteiro nao negativo
    printf("\nDigite o segundo numero inteiro nao negativo: ");
    scanf("%d", &numero2);

    // Imprimir na tela o valor do fatorial do primeiro numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "for"
    printf ("\nFatorial de %d usando o laco for= %d \n", numero1, fatorial_for(numero1));

    // Imprimir na tela o valor do fatorial do segundo numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "for"
    printf ("\nFatorial de %d usando o laco for= %d \n", numero2, fatorial_for(numero2));

    // Imprimir na tela o valor do fatorial da diferenca entre o primeiro numero e
    // o segundo numero, que sao inteiros nao negativo, usando a estrutura de 
    // repeticao com o operador "for"
    printf ("\nFatorial de %d usando o laco while= %d \n", numero1-numero2, fatorial_for(numero1-numero2));

    // Imprimir na tela o valor do fatorial do arranjo dos dois numeros inteiros nao
    // negativos, usando a estrutura de repeticao com o operador "for"
    arranjo = fatorial_for( numero1 ) / ( fatorial_for( numero2 ) * fatorial_for( numero1 - numero2 ) );
    printf ("\nCombinacaa de %d, %d a %d, e= %d \n", numero1, numero2, numero2, arranjo);

    return 0;
} // Fim da funcao MAIN

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "for" e iterador crescente
int fatorial_for(int n)
{
    int i;
    int f = 1;

    for (i = 1; i <= n; i++)
        f *= i;

    return f;
} // Fim da funcao "fatorial_for"

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "while"
int fatorial_while(int n)
{
    int i = n;
    int f = 1;

    while (i > 0)
    {
        f *= i;
        i--;
    }

    return f;
} // Fim da funcao "fatorail_while"

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de recrusividade
int fatorial_rec(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return n * fatorial_rec(n - 1);
    }
} // Fim da funcao FATORIAL RECURSIVA
