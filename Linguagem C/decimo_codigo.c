/* 
 * Decimo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

/*  Programa que converte uma medida em
 *  1. metros para pes e polegadas
 *  2. pes e polegadas para metros
 *  Esse algoritmo usa a estrutura de condicional "switch"
*/
int main(void) {
    // Alocando memoria para variaveis inteiras
    int pes, escolha;
    // Alocando memoria para variaveis reais
    float altura, polegadas, resto;
    // Alocando e designando valor a variaveis reais
    float converson_pes = 30.48, conversor_pol = 2.54;

    // Informando ao usuario as opcoes de conversao para escolha
    printf("- Conversor de Metros para Pes e Polegadas: digite 1;\n");
    printf("- Conversor de Pes e Polegadas para Metros: digite 2;\n");
    printf("Escolha qual tipo de conversao desejada: ");
    // Designando valor digitado pelo usuario a variavel "escolha"
    scanf("%d",&escolha);

    // Estrutura de condicional usando o comando "switch"
    switch ( escolha )
    {
        case 1/* expressao constante */:
            /* code */
            printf("\n\nDigite a altura: ");
            scanf("%f", &altura);

            altura = 100 * altura;

            pes = (int)(altura / converson_pes);
            resto = (altura / converson_pes) - pes;
            polegadas = (resto * converson_pes) / conversor_pol;

            printf("\n\nAltura e: %d ft %.1f pol", pes, polegadas);

            break;
    
        case 2/* expressao constante */:
            /* code */
            printf("\n\nDigite a altura referente a Pes: ");
            scanf("%d",&pes);
            printf("\n\nDigite a altura referente a Polegadas: ");
            scanf("%f",&polegadas);

            altura = ((pes * converson_pes) + (polegadas * conversor_pol) ) / 100;

            printf("\n\nAltura e: %.1f metro(s)", altura);

            break;
        
        default:
            printf("\nVoce escolheu uma alternativa inexistente!");
            break;
    }

/*  Uma segunda opcao desse codigo usando a estrutura de condicional "if"

    if(escolha == 1) {
        printf("\n\nDigite a altura: ");
        scanf("%f", &altura);

        altura = 100 * altura;

        pes = (int)(altura / converson_pes);
        resto = (altura / converson_pes) - pes;
        polegadas = (resto * converson_pes) / conversor_pol;

        printf("\n\nAltura e: %d ft %.1f pol", pes, polegadas);
    }
    if(escolha == 2) {
        printf("\n\nDigite a altura referente a Pes: ");
        scanf("%d",&pes);
        printf("\n\nDigite a altura referente a Polegadas: ");
        scanf("%f",&polegadas);

        altura = ((pes * converson_pes) + (polegadas * conversor_pol) ) / 100;

        printf("\n\nAltura e: %.1f metro(s)", altura);
    }*/
    return 0;
}