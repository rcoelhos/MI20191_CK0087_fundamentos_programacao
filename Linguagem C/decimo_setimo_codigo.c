/* 
 * Decimo setimo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

// subrotina que calcula a soma e o produto entre dois numeros inteiros,
// usando a ponteiros para armazenar o endereco de memoria dos resultados
void SomaProd(int a, int b, int *soma, int *produto)
{   // Caso as variaveis referentes a soma e a multiplicacao dentro da
    // subrotina, esses valores nao poderam ser recuperados fora dela
    //int soma, produto;

     *soma = a + b;
     *produto = a * b;

     //return (produto, soma);
}

int main()
{   // Declaracao das variaveis do algoritmo
    int a = 7, b = 5;
    int soma, produto;

    // Chamada da subrotina com os quatro parametros, sendo os dois primeiros
    // os valores a serem somados e multiplicados, enquanto os dois ultimos
    // sao as variaveis que guardaram esses resultados
    SomaProd( a , b , &soma, &produto );

    printf("\nO resultado da soma entre %d e %d eh: %d", a, b, soma);
    printf("\nO resultado do produto entre %d e %d eh: %d", a, b, produto);

    return 0;
}