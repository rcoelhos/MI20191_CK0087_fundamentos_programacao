/* 
 * Decimo segundo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Declaracao dos prototipos
int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"
int fatorial_while( int ); //essa linha chama-se prototipo com o laco usando "while"

int main(void) {
    int numero; //declaracao das variaveis

    // Solicitar ao usuario um numero inteiro nao negativo
    printf("Digite um numero inteiro nao negativo: ");
    scanf("%d", &numero);

    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "for"
    printf ("\nFatorial de %d usando o laco for= %d \n", numero, fatorial_for( numero ));

    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "while"
    printf ("\nFatorial de %d usando o laco while= %d \n", numero, fatorial_while( numero ));

    return 0;
} // Fim da funcao MAIN

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "for" e iterador crescente
int fatorial_for( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;
} // Fim da funcao "fatorial_for"

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "while"
int fatorial_while( int n ) {
    int i = 1;
    int f = 1;

    while(i <= n)
    {
        f *= i;
        i++;
    }
    
    return f;
} // Fim da funcao "fatorial_while"