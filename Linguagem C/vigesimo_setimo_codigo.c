#include <stdio.h>

#define M 5
#define N 5

int main()
{
    int matriz[M][N];
    //int matriz[M][N] = {{1,2,3,4,5}, {3,5,7,9,11}, {2,4,6,8,10}};
    //int matriz[M][N] = {1,2,3,4,5,3,5,7,9,11,2,4,6,8,10};
    //matriz[M][N] = {{1,2,3,4,5}, {3,5,7,9,11}, {2,4,6,8,10}, {1,1,1,1,1}};

    for(int ii = 0; ii < M; ii++)
    {
        for(int jj = 0; jj < N; jj++)
        {
            matriz[ii][jj] = (ii + jj) % 5;
        }
    }

    for(int ii = 0; ii < M; ii++)
    {
        printf("Elementos da linha %d\n", ii);
        for(int jj = 0; jj < N; jj++)
        {
            printf("%d\t",matriz[ii][jj]);
        }
        printf("\n");
    }

    return 0;
}