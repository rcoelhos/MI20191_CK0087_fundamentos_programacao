/* 
 * Vigesimo primeiro codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida
#include <math.h>  // biblioteca para operacoes elementares de matematica

// Subrotina para padronizar um tipo especifico de impressao na tela dos
// resultados obtidos pelo programa principal, que tambem pode ser usada
// dentro de outra subrotina
void imprime(float a)
{
    static int n = 1;

    printf(" %f\t", a);
    if((n % 5) == 0)
    {
        printf("\n");
    }
    n++;
}

int main()
{
    float valor = 1.0;

    for(int i = 0; i < 20; i++)
    {
        imprime( i * 1.5 * valor );
    }

    return 0;
}