/* 
 * Quarto codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */ 

#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Programa que calcula a area e perimetro a partir da altura e largura de um retangulo,
// solicitando altura e largura ao usuario
int main(void) {
    // Alocando memoria para qutro variaveis inteiras
    int altura;            // variavel que armazenara na variavel "altura" do retangulo
    int largura;           // variavel que armazenara na variavel "largura" do retangulo
    int area, perimetro;   // variaveis que armazenarao os calculos de area e perimetro

    // Funcoes da biblioteca <stdio.h> para entrada e saida de informacao
    printf("Digite a altura: ");    // impressao de um texto na tela
    scanf("%d", &altura);           // armazenar valor digitado pelo usuario na variavel "altura"
    printf("Digite a largura: ");   // impressao de um texto na tela
    scanf("%d", &largura);          // armazenar valor digitado pelo usuario na variavel "largura"

    // Calculo matematico para obter o valor da variavel "area" desse retangulo
    area = altura * largura;
    // Calculo matematico para obter o valor da variavel "perimetro" desse retangulo
    perimetro = 2 * ( altura + largura );

    // Funcao da biblioteca <stdio.h> para imprimir informacao na tela
    printf("area do retangulo = %d \n", area);              // impressao do valor da variavel "area"
    printf("perimetro do retangulo = %d \n", perimetro);    // impressao do valor do variavel "perimetro"

    return 0;
}