#include <stdio.h>
#include <stdlib.h>

// Prototipo da subrotina de produto vetorial
//float* ProdutoVetorial(float*, float*, float*);
void ProdutoVetorial( float*, float*, float* );
// Prototipo para imprimir as componentes do vetor
void ImprimirVetor(float*);

int main()
{
    // Declaracao dos vetores
    float primeiroVetor[3] = {1.5, 3.0, 2.5}; 
    float segundoVetor[3] = {5.8, 2.7, 3.14};
    float resultadoProdVet[3];

    ImprimirVetor( primeiroVetor );
    ImprimirVetor( segundoVetor );

    //resultadoProdVet = 
    ProdutoVetorial( primeiroVetor, segundoVetor, resultadoProdVet );

    ImprimirVetor( resultadoProdVet );

    return 0;
}

//float* ProdutoVetorial(float* u, float* v, float* r)
void ProdutoVetorial(float* u, float* v, float* r)
{
    // Declaracao do vetor
    //float* vetorAuxiliar = (float *)malloc( 3 * sizeof(float) );

    // Atribuicao de valores para as posicoes do vetor
    r[0] = ( u[1]*v[2] ) - ( v[1]*u[2] );
    r[1] = ( u[2]*v[0] ) - ( v[2]*u[0] );
    r[2] = ( u[0]*v[1] ) - ( v[0]*u[1] );

    //return vetorAuxiliar;
}

// Subrotina para imprimir os valores de todas as componentes do vetor
void ImprimirVetor(float* vetor)
{
    int posicao; //inteiro que representa a posicao do vetor

    for(posicao = 0; posicao < 3; posicao++)
    {
        printf("%.2f\t", vetor[ posicao ]);
    }
    printf("\n");
}