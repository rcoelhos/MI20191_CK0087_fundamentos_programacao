#include <stdio.h>  // biblioteca para operacoes de entrada e saida
#include <string.h> // biblioteca para manipulacao de 'strings'

// Descricao dos prototipos para manipulacao de cadeias de caracteres
int comprimento ( char * );         // calcula o comprimeiro da cadeia
void copia ( char *, char * );      // copia conteudo de uma cadeia em outra (sobescreve)
void concatena ( char *, char * );  // copia conteudo de uma cadeia em outra (acrescenta)
int compara ( char *, char * );     // compara o conteudo das duas cadeias

int main()
{
    // Criando variaveis, que sao cadeias de caracteres (vetores de char), com
    // comprimento maximo de cada variavel para 30 caracteres
    char cidadeOrigem[30] = "Rio de Janeiro";
    char cidadeDestino[30] = "Rio Grande do Norte";
    char cidadeAuxiliar[30] = "Rio Grande do Sul";
    // alocar memoria para uma variavel com valor inteiro
    int resposta;

    printf("%s\n", cidadeOrigem);
    printf("O tamanho dessa string eh: %d\n", strlen( cidadeOrigem ));
    
    printf("%s\n", cidadeDestino);
    printf("O tamanho dessa string eh: %d\n", strlen( cidadeDestino ));

    printf("%s\n", cidadeAuxiliar);
    printf("O tamanho dessa string eh: %d\n", strlen( cidadeAuxiliar ));

    //strcpy( cidadeDestino, cidadeAuxiliar );
    //printf("%s\n", cidadeDestino);

    //strcat( cidadeOrigem, cidadeDestino );
    //printf("%s\n", cidadeOrigem);

    resposta = strcmp( cidadeDestino, cidadeAuxiliar );
    printf("O resultado da comparacao eh: %d\n", resposta);

    resposta = strncmp( cidadeDestino, cidadeAuxiliar, 15 );
    printf("O resultado da comparacao dos 15 primeiros caracteres eh: %d\n", resposta);

    return 0;
}

int comprimento ( char * s ) {
    int i;
    int n = 0;
    for (i = 0; s[i] != '\0'; i++) 
    {
        n++;
    }
    return n;
}

void copia ( char * dest , char * orig )
{
    int i;

    for (i = 0; orig [i] != '\0'; i++) {
        dest [i] = orig [i];
    }
    dest [i] = '\0';
}

void concatena ( char * dest, char * orig )
{
    int ii = 0, jj;

    while (dest[ ii ] != '\0') ii++;

    for (jj = 0; orig[ jj ] != '\0'; jj++)
    {
        dest [ ii ] = orig[ jj ];
        ii++;
    }

    dest[ ii ] = '\0';
}

int compara ( char * s1 , char * s2)
{
    int i;

    for (i = 0; s1[ i ] != '\0' && s2[ i ] != '\0'; i++) 
    {
        if (s1[i]<s2[i])
            return -1;
        else if (s1[i]>s2[i])
            return 1;
    }

    if (s1[i] == s2[i])
        return 0;
    else if (s2[i]!= '\0')
        return -2;
    else
        return 2;
}