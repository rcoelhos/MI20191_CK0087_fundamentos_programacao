/* 
 * Decimo terceiro codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

// Declaracao dos prototipos
int fatorial_for( int ); //essa linha chama-se prototipo com o laco usando "for"
int fatorial_while( int ); //essa linha chama-se prototipo com o laco usando "while"
int fatorial_rec( int ); //essa linha chama-se prototipo com recursividade

int main(void) {
    int numero, fatorial; //declaracao das variaveis

    // Solicitar ao usuario um numero inteiro nao negativo
    printf("Digite um numero inteiro nao negativo: ");
    scanf("%d", &numero);

    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "for"
    fatorial = fatorial_for( numero );
    printf ("\nFatorial de %d usando o laco for= %d \n", numero, fatorial);

    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo,
    // usando a estrutura de repeticao com o operador "while"
    fatorial = fatorial_while( numero );
    printf ("\nFatorial de %d usando o laco while= %d \n", numero, fatorial);

    // Imprimir na tela o valor do fatorial de um numero inteiro nao negativo,
    // usando a estrutura de recursividade
    fatorial = fatorial_rec( numero );
    printf ("\nFatorial de %d usando recursividade= %d \n", numero, fatorial);

    return 0;
} // Fim da funcao MAIN

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "for" e iterador crescente
int fatorial_for( int n ) {
    int i;
    int f = 1;

    for (i = 1; i <= n; i++) f *= i;

    return f;
} // Fim da funcao "fatorial_for"

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de repeticao com o operador "while"
int fatorial_while( int n ) {
    int i = n;
    int f = 1;

    while(i > 0)
    {
        f *= i;
        i--;
    }
    
    return f;
} // Fim da funcao "fatorail_while"

// Funcao auxiliar para calcular o fatorial de um numero inteiro nao negativo,
// usando a estrutura de recrusividade
int fatorial_rec( int n ) {
    if (n == 0) 
    {
        return 1;
    } 
    else
    {
        return n * fatorial_rec( n - 1 );
    }
} // Fim da funcao FATORIAL RECURSIVA
