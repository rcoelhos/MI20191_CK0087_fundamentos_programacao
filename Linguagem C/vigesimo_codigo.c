/* 
 * Vigesimo codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h>  // biblioteca para operacoes de entrada e saida
#include <math.h>   // biblioteca para operacoes elementares de matematica

// Prototipo da subrotina soma_dobro
int soma_dobro (int *a, int *b);

int main () {
    int x, y, res;
    printf (" Digite o primeiro numero : \n");
    scanf ("%d" ,&x);
    printf (" Digite o segundo numero : \n");
    scanf ("%d" ,&y);
    res = soma_dobro (&x ,&y);
    printf ("A soma do dobro de %d e %d = %d \n",x,y, res);
    return 0;
}

int soma_dobro (int *a, int *b) {
    int soma, aux=3;
    *a = 2 * (*a); // Atualizacao da variavel a qual o ponteiro a está apontando
    *b = 2 * (*b);
    soma = *a + *b;
    return soma ;
}