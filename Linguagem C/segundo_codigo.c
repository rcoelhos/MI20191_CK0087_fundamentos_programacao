/*  
 *  Segundo codigo em Linguagem C
 *  Curso: Matematica Industrial
 *  Disciplina: Fundamento de Programacao
 *  Prof.: Ricardo Coelho
 *  Versao 0.2
*/

#include <stdio.h> // Biblioteca padrao da Linguagem C para funcoes de entrada e saida

int main(void) {
    // Alocacao de memoria para cinco variaveis inteiras
    int a, b;
    int c = 23;     // Alocacao de memoria e designacao de valor a variavel c inteira
    int d = c + 4;  // Alocacao de memoria e designacao 
    int e, f;

    // Testes logicos usando conectivos de "ou" e "e", respectivamente
    a = (c < 20) || (d > c);
    b = (c < 20) && (d > c);

    // Funcao da piblioteca <stdio.h> para imprimir resultado na tela
    printf("a = %d \n", a);
    printf("b = %d \n", b);

    // Conversao de valor float em valor inteiro a ser armazenado em variaveis inteiras
    e = (int) 3.8;
    f = (int) 3.5 % 2;      // O operador "%" e usado para devolver o resto da divisao de 3.5 por 2

    // Funcao da piblioteca <stdio.h> para imprimir resultado na tela
    printf("e = %d \n", e);
    printf("f = %d, \n", f);

    return 0;
}