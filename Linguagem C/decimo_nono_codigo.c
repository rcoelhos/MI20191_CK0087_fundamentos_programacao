/* 
 * Decimo nono codigo em Linguagem C
 * Curso: Matematica Industrial
 * Disciplina: Fundamentos de Programacao
 * Prof.: Ricardo Coelho
 * Versao 0.2
 */

// Declaracao das bibliotecas a serem usadas
#include <stdio.h> // biblioteca para operacoes de entrada e saida

//Declaracao de variaveis globais, que poderao ser recuperados seus valores
// por qualquer subrotina e pelo programa principal a qualquer momento
int soma, produto;

void SomaProd(int a, int b)
{
     soma = a + b;
     produto = a * b;
}

int main()
{
    int a = 7, b = 5;

    SomaProd( a , b );

    soma = a - b;
    produto = a / b;

    printf("\nO resultado da soma entre %d e %d eh: %d", a, b, soma);
    printf("\nO resultado do produto entre %d e %d eh: %d", a, b, produto);

    return 0;
}