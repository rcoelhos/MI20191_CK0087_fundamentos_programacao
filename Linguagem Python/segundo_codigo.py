# Segundo codigo em Linguagem Python

c = 23
d = c + 4

a = (c < 20) or (d > c) # ou posso substituir | por or
b = (c < 20) and (d > c) # ou posso substituir & por and

print("a = ", a)
print("b = ", b)